//Ex1 
// Đầu vào : Lương 1 ngày 100.000vnđ
// Các bước xử lí: -Tạo biến a là số tiền lương và b là số ngày làm và gán giá trị cho cả a và b  
//                 -Tạo biến result1 là kết quả 
//                 - Sử dụng công thức lấy số lương nhân với số ngày làm  

// Đầu ra :in ra kết quả tiền lương 

var a = 100000;
var b = 30;
result1 = a * b;
console.log("🚀 ~ file: index.js ~ line 12 ~ result1", result1)



// Ex2
// Đầu vào: 5 số thưc abcde
// Các bước xử lí: -Tạo biến a là số thứ nhất và b là số thứ 2 và c là số thứ 3 , d là số thứ 4 , e là số thứ 5   
//                 -Tạo biến result2 là trung bình cộng của 5 số thực
//                 - Sử dụng công thức trung bình của 5 số là cộng tất cả lại chia cho 5   

// Đầu ra : In ra giá trị trung bình của 5 số này

var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;
result2 = (a + b + c + d + e) / 5;
console.log("🚀 ~ file: index.js ~ line 30 ~ result2", result2)

// Ex3
// Đầu vào: 2 
// Các bước xử lí: -Tạo biến a1 là số tiền USD cần đổi sang VNĐ
//                 -Tạo biến result3 là kết quả 
//                 - Sử dụng công thức nhân 2 số với nhau 

// Đầu ra : In ra số tiền quy đổi sang vnđ

var a1 = 3;
result3 = a1 * 23.500;
console.log("🚀 ~ file: index.js ~ line 42 ~ result3 ", result3)

// Ex4
// Đầu vào: chiều dài a4 =2 , chiều rộng b4 =3
// Các bước xử lí: -Tạo biến a4 là chiều dài , b 4 là chiều rộng 
//                 -Tạo biến result4a là chu vi hình chữ nhật
//                 -Tạo biến result4b là diện tích hình chữ nhật
//                 - Sử dụng công thức chu vi và diện tích hình chữ nhật

// Đầu ra : In ra chu vi , diện tích hình chữ nhật 


var a4 = 2;
var b4 = 3;
result4a = (2 + 3) * 2;
console.log("🚀 ~ file: index.js ~ line 57 ~ result4a", result4a)
result4b = 2 * 3;
console.log("🚀 ~ file: index.js ~ line 59 ~ result4b", result4b)

// Ex5
// Đầu vào: 2 ký số 12 
// Các bước xử lí: -Tạo biến a5 là số hàng chục
//                 -Tạo biến b5 là số hàng đơn vị
//                 -Tạo biến result5 là tổng 2 ký số


// Đầu ra : In ra result5


n = 12;
var a5 = Math.floor(n / 10) % 10;
var b5 = n % 10;
result5 = a5 + b5;
console.log("🚀 ~ file: index.js ~ line 75 ~ result5", result5)



